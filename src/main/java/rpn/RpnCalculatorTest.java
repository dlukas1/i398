package rpn;

import org.junit.jupiter.api.Test;
import stack.Stack;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class RpnCalculatorTest {

    @Test
    public void newCalculatorHasZeroInItsAccumulator() {
        RpnCalculator c = new RpnCalculator();
        assertThat(c.getAccumulator(), is(0));
    }

    @Test
    public void setAccumulator() {
        RpnCalculator c = new RpnCalculator();
        c.setAccumulator(5);
        assertThat(c.getAccumulator(), is(5));

    }

    @Test
    public void checkStackAfterPush(){
        RpnCalculator c = new RpnCalculator();
        c.setAccumulator(5);
        c.enter();
        assertThat(c.getAccumulator(), is(5));
        //assertThat(c.stack.pop(), is(5));
    }

    @Test
    public void testPlus(){
        RpnCalculator c = new RpnCalculator();
        c.setAccumulator(3);
        c.enter();
        c.setAccumulator(4);
        c.plus();
        assertThat(c.getAccumulator(), is(7));
    }

}

class RpnCalculator {

    static int accu = 0;
    public Stack stack;

    public int getAccumulator() { return accu; }

    public void setAccumulator (int newNumber){ accu += newNumber; }

    public void enter(){ stack.push(accu);}

    public void plus(){ accu = stack.pop()+accu; }

    public void minus(){accu = stack.pop()-accu;}

    public void multiply() {accu = stack.pop()*accu;}



}