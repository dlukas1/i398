package gameOfLife;

import java.util.Arrays;


public class Frame {

    public Frame(){};

   //Create empty board
   public static String[][] board = new String[10][10];

    //Fill board with " - "
   public void initializeBoard(String[][]board){
       for (int i = 0; i < board.length; i++) {
           for (int j = 0; j < board[i].length; j++) {
               board[i][j] = "-";
           }
       }
   }

    // Marks cell with coordinates x and y as alive
    public static void markAlive(int x, int y) { board[x][y] = "X"; }

    //Count alive cells & return count
    public int countAlive (String[][]toCount)
    {
        int count = 0;
        for (int i = 0; i < toCount.length; i++)
        {
            for (int j = 0; j < toCount.length; j++) { if (toCount[i][j]=="X") count++; }
        }
        return count;
    }


    public boolean equals(String[][] arr1, String [][] arr2) {

        if(Arrays.deepEquals(arr1, arr2)){return true;}
        return false;
    }

    public boolean isStable(String[][] arr){
       if (Arrays.deepEquals(arr, nextFrame(arr))) {return true;}
       return false;
    }

    public Integer getNeighbourCount(int x, int y) {
        // Returns cells neighbor count
        // Possible range is 0-8
        int aliveNeighbors = 0;
        //OutOfBoundException form here
        try {
            if (board[x][y+1] == "X") aliveNeighbors++;
            if (board[x][y-1] == "X") aliveNeighbors++;
            if (board[x+1][y] == "X") aliveNeighbors++;
            if (board[x-1][y] == "X") aliveNeighbors++;
            if (board[x+1][y+1] == "X") aliveNeighbors++;
            if (board[x+1][y-1] == "X") aliveNeighbors++;
            if (board[x-1][y+1] == "X") aliveNeighbors++;
            if (board[x-1][y-1] == "X") aliveNeighbors++;
        } catch (Exception e){}


        return aliveNeighbors;
    }

    // True if cell with coordinates x and y is alive
    public boolean isAlive(int x, int y) {
        if (board[x][y] == "X"){return true;}  return false;
    }


    public String[][] nextFrame(String[][]currentFrame)
    {
       String[][] newFrame = new String[10][10];
       initializeBoard(newFrame);

        for (int i = 0; i < currentFrame.length; i++) {
            for (int j = 0; j < currentFrame.length; j++) {
                if ( currentFrame [i][j] == "X")
                {
                    //Die if not enought or too much nabers
                    if (getNeighbourCount(i,j) == 2 || getNeighbourCount(i,j)==3) {
                        newFrame [i][j] = "X";}
                }

                //Resurrect when has 3 nabers
                else
                {if(getNeighbourCount(i,j)== 3)  {newFrame [i][j] = "X";} }
            }
        }
        // Create new frame that has the same size.

        // Calculate the new state of each cell from the old frame
        // and mark it in the new frame.
        // Return new frame.
        // Returns next frame
        return newFrame;
    }



    public static <T> String printBoard (T[][] x) {
        final String vSep = "\n";
        final String hSep = "";
        final StringBuilder sb = new StringBuilder();

        if(x != null)
            for(int i = 0; i < x.length; i++) {
                final T[] a = x[i];
                if(i > 0) {
                    sb.append(vSep);
                }
                if(a != null)
                    for(int j = 0; j < a.length; j++) {
                        final T b = a[j];
                        if(j > 0) {
                            sb.append(hSep);
                        }
                        sb.append(b);
                    }
            }
        return sb.toString();
    }

    public static void clearBoard(){

        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j <board.length ; j++) {
                board[i][j] = "-";
            }
        }
    }




}
