package gameOfLife;



import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.jupiter.api.Test;

    @SuppressWarnings("unused")
    public class GameOfLifeTest {

        //Check is board created
        @Test
        public void checkTable(){
            Frame g = new Frame();
            g.initializeBoard(g.board);
            g.printBoard(g.board);
            assertThat(g.board.length, is(10));
        }


        // Mark cells as alive and check
        @Test
        public void markAliveAndCheck(){
            Frame g = new Frame();
            g.initializeBoard(g.board);
            g.markAlive(1,1);

            assertThat(g.board[1][1], is("X"));
        }

        //// Count alive neighbors.
        @Test
        public void countAliveNeighors(){
            Frame g = new Frame();

            g.initializeBoard(g.board);
            g.markAlive(1,1);
            g.markAlive(1,2);
            g.markAlive(1,0);
            g.markAlive(2,0);
            g.markAlive(2,1);
            g.markAlive(2,2);
            g.markAlive(0,0);
            g.markAlive(0,1);
            g.markAlive(0,2);

            assertThat(g.getNeighbourCount(1,1),is(8));
        }

        @Test
        public void checkCounter(){
            Frame g = new Frame();

            g.initializeBoard(g.board);
            g.markAlive(1,1);
            g.markAlive(3,3);
            g.markAlive(5,5);
            assertThat(g.countAlive(g.board), is(3));


        }

        //3 live cell should die after clearBoard
        @Test
        public void fillAndClear(){
            Frame g = new Frame();
            g.initializeBoard(g.board);
            g.markAlive(1,1);
            g.markAlive(3,3);
            g.markAlive(5,5);
            g.clearBoard();
            assertThat(g.countAlive(g.board),is(0) );
        }


        //Mark 3 alive so they'll die in next frame
        @Test
        public void checkCellsToDieInNextGen(){
            Frame g = new Frame();

            g.initializeBoard(g.board);
            g.markAlive(1,1);
            g.markAlive(3,3);
            g.markAlive(5,5);

            g.board =  g.nextFrame(g.board);
            assertThat(g.countAlive(g.board), is(0));


        }

        //Mark 3 in a row, nextGen 1 stay alive and 1 resurrect
        @Test
        public void mark3survive1new1(){
            Frame g = new Frame();
            g.initializeBoard(g.board);

            g.markAlive(0,0);
            g.markAlive(0,1);
            g.markAlive(0,2);

            g.board = g.nextFrame(g.board);
            assertThat(g.countAlive(g.board), is(2));

        }


        //Checking stable configuration
        @Test
        public void checkStablePound(){
            Frame g = new Frame();

            g.initializeBoard(g.board);

            g.markAlive(2,3);
            g.markAlive(2,4);
            g.markAlive(3,5);
            g.markAlive(4,5);
            g.markAlive(5,4);
            g.markAlive(5,3);
            g.markAlive(4,2);
            g.markAlive(3,2);

            String [][] nextFrame = g.nextFrame(g.board);
            assertThat(g.equals(g.board, nextFrame), is(true));

        }

        @Test
        public void checkStableBlock(){
            Frame g = new Frame();
            g.initializeBoard(g.board);

            g.markAlive(3,2);
            g.markAlive(3,3);
            g.markAlive(4,2);
            g.markAlive(4,3);

            assertThat(g.equals(g.board, g.nextFrame(g.board)) ,is(true));

        }

        @Test
        public void checkUnstableLine(){
            Frame g = new Frame();

            g.initializeBoard(g.board);

            g.markAlive(1,1);
            g.markAlive(2,2);
            g.markAlive(3,3);

            assertThat(g.equals(g.board, g.nextFrame(g.board)), is(false));
        }

        //1st & 3rd frame are equals and 2nd frame have 8 cells alive
        @Test
        public void checkPulseCountPlus2DifferentPosition(){
            /*
            ------ ------ ------
            -XX--- -XX--- -XX---
            -X---- -XX--- -X----
            ----X- ---XX- ----X-
            ---XX- ---XX- ---XX-
            ------ ------ ------ jne..
             */

            Frame g = new Frame();
            g.initializeBoard(g.board);

            g.markAlive(2,2);
            g.markAlive(2,3);
            g.markAlive(3,2);
            g.markAlive(4,5);
            g.markAlive(5,4);
            g.markAlive(5,5);

            String[][]temp = g.board;

            for (int i = 0; i < 4; i++) {
                g.board = g.nextFrame(g.board);
                System.out.println(g.printBoard(g.board));
            }
            assertThat(g.equals(temp, g.board),is(true));
        }

        //4 live cells in a line transform to stable beehive in 3rd generation
        @Test
        public void lineToBeehive(){
            /*
            ----------
            --XXXX----
            ----------

            ----------
            ---XX-----
            ---XX-----
            ---XX-----
            ----------

            ----------
            ---XX-----
            --X--X----
            ---XX-----
            ----------
             */
            Frame g = new Frame();
            g.initializeBoard(g.board);

            g.markAlive(2,2);
            g.markAlive(2,3);
            g.markAlive(2,4);
            g.markAlive(2,5);

            String[][] temp = g.board;
            for (int i = 0; i < 4; i++) {
                g.board = g.nextFrame(g.board);
            }
            assertThat(g.isStable(g.board), is(true));
        }


        //Glider is moving and finally transforms to block
        @Test
        public void gliderSameAliveDiffPosition(){

            /*
            --X-------
            ---X------
            -XXX------

            -X-X------
            --XX------
            --X-------

            ---X------
            -X-X------
            --XX------
             */
            Frame g = new Frame();
            g.initializeBoard(g.board);

            g.markAlive(2,2);
            g.markAlive(3,3);
            g.markAlive(4,3);
            g.markAlive(4,1);
            g.markAlive(4,2);

            //System.out.println(g.printBoard(g.board));
            for (int i = 0; i <20 ; i++) {
                g.board = g.nextFrame(g.board);
               // System.out.println(g.printBoard(g.board));

            }   assertThat(g.isStable(g.board), is(true));
        }


        //Frog transforms to block in 4rd generation
        @Test
        public void frogToBlock(){
            Frame g = new Frame();
            g.initializeBoard(g.board);

            /*
            (1st gen)
            -XXX------
            XXX-------
            (4th gen)
            --XX------
            --XX------
             */
            g.markAlive(3,1);
            g.markAlive(3,2);
            g.markAlive(3,3);
            g.markAlive(4,0);
            g.markAlive(4,1);
            g.markAlive(4,2);

            for (int i = 0; i <4 ; i++) {
                g.board = g.nextFrame(g.board);
            }
            assertThat(g.countAlive(g.board), is(4));
            assertThat(g.equals(g.board, g.nextFrame(g.board)), is(true));
        }

        //Set of 4 cells transforms into different figures and finally to traffic light
        @Test
        public void trafficLight (){
            Frame g = new Frame();
            g.initializeBoard(g.board);

            g.markAlive(3,4);
            g.markAlive(4,3);
            g.markAlive(4,4);
            g.markAlive(4,5);

            System.out.println(g.printBoard(g.board));
            g.board = g.nextFrame(g.board);

            for (int i = 0; i < 20; i++) {
                System.out.println(g.printBoard(g.board));
                g.board = g.nextFrame(g.board);
            }
            assertThat(g.countAlive(g.board),is(6));
        }








    }
