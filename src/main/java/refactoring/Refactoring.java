package refactoring;

import java.util.*;

public class Refactoring {

    //1a Rename Method
    public int getNextInvoiceNumber() {
        return ++invoiceNumber;
    }

    //1b Rename Method
    public void addFilledOrders(List<Order> orderList) {
        for (Order order : orders) {
            if (order.isFilled()) {
                orderList.add(order);
            }
        }
    }

    //2a Extract Method
    public void printStatementFor(Long invoiceId) {
        List<InvoiceRow> invoiceRows = dao.getInvoiceRowsFor(invoiceId);
        printInvoiceRows(invoiceRows);
        double total = 0;
        for (InvoiceRow invoiceRow : invoiceRows) { total += invoiceRow.getAmount(); }
        printValue(total);
    }

    //2b Extract Method, Remove Duplication
    public String getItemsAsHtml() {
        String result = "";
        result += "<ul>";
        for (int i = 0; i < 4; i++) { result += createTag("li",items.get(i)); }
        result += "</ul>";
        return result;
    }
    // Submetgod for getItemsAsHtml()
    public String createTag(String tag, String element){
        String tagged = "<" + tag + ">" + element + "</" + tag + ">";
        return tagged;
    }

    //3 Inline Temp
    public boolean isSmallOrder() { return (order.getTotal() > 100); }

    // 4 Replace Temp with Query, Replace Magic Number with Symbolic Constant
    public void printPrice() {

        final double vat = 1.2;
        System.out.println("Price not including VAT: " + getBasePrice());
        System.out.println("Price including VAT: " + getBasePrice() * vat);
    }


    // 5 Introduce Explaining Variable
    public void calculatePayFor(Job job) {
        // on holiday at night
        final boolean isWeekend = (job.day ==6 ||job.day ==7);
        final  boolean isOvertime = (job.hour > 20 || job.hour < 7);
        if (isWeekend && isOvertime)  { }
    }

    // 6  Decompose Conditional
    public boolean canAccessResource(SessionData sessionData) {
        // is admin and has preferred status
        final boolean isAdmin = sessionData.getCurrentUserName().equals("Admin") ||
                sessionData.getCurrentUserName().equals("Administrator");
        final boolean isStatusXorY = sessionData.getStatus().equals("preferredStatusX")
                || sessionData.getStatus().equals("preferredStatusY");
        return ( isAdmin && isStatusXorY );
    }

    // 7 Introduce Parameter Object(s)
    public void drawLines() {
        Space space = new Space();
        Point sp1 = new Point(12, 3, 5);
        Point fp1 = new Point(2, 4, 6);
        Point sp2 = new Point(2, 4, 6);
        Point fp2 = new Point(0, 1, 0);
        space.drawLine(sp1,fp1);
        space.drawLine(sp2,fp2);

    }

    // 8 Remove Selector Argument
    public int calculateWeeklyPayWithOvertime(int hoursWorked){
        int straightTime = Math.min(40, hoursWorked);
        int overTime = Math.max(0, hoursWorked - straightTime);
        int straightPay = straightTime * hourRate;
        double overtimeRate =  hourRate * 1.5;
        int overtimePay = (int) Math.round(overTime * overtimeRate);
        return straightPay + overtimePay;

    }

    public int calculateWeeklyPayWithoutOvertime(int hoursWorked){
        return hoursWorked * hourRate;
    }

    // //////////////////////////////////////////////////////////////////////////

    // Helper fields and methods.
    // They are here just to make code compile

    private List<String> items = Arrays.asList("1", "2", "3", "4");
    private int hourRate = 5;
    int invoiceNumber = 0;
    private List<Order> orders = new ArrayList<>();
    private Order order = new Order();
    private Dao dao = new SampleDao();
    private double price = 0;

    void justACaller() {
        getNextInvoiceNumber();
        addFilledOrders(null);
    }

    private void printValue(double total) {
    }

    private void printInvoiceRows(List<InvoiceRow> invoiceRows) {
    }

    public class Point {

        public Point(int x, int y, int z) {
        }

    }

    class Space {
        public void drawLine(Point p1, Point p2) {
        }

    }

    interface Dao {
        List<InvoiceRow> getInvoiceRowsFor(Long invoiceId);
    }

    class SampleDao implements Dao {
        @Override
        public List<InvoiceRow> getInvoiceRowsFor(Long invoiceId) {
            return null;
        }
    }

    class Order {
        public boolean isFilled() {
            return false;
        }

        public double getTotal() {
            return 0;
        }
    }

    class InvoiceRow {
        public double getAmount() {
            return 0;
        }
    }

    class Job {
        public int hour;
        public int day;
    }

    private double getBasePrice() {
        return price;
    }

    private class SessionData {

        public String getCurrentUserName() {
            return null;
        }

        public String getStatus() {
            return null;
        }

    }

}
