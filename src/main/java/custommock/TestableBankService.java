package custommock;

import common.BankService;
import common.Money;

public class TestableBankService implements BankService {

    Money wMoney;
    Money dMoney;

    @Override
    public void withdraw(Money money, String fromAccount) {

        System.out.println("withdraw called with: " + money + " and " + fromAccount);
         wMoney = money;

    }

    @Override
    public void deposit(Money money, String toAccount) {
        System.out.println("deposit called with: " + money + " and " + toAccount);
         dMoney = money;
    }

    public boolean wasWithdrawCalledWith(Money money, String account) {

        if (wMoney.equals(money))return true;
        return false;
        // here you have to compare current arguments with the ones remembered
    }

    public boolean wasDepositCalledWith(Money money, String account) {

        if (dMoney.equals(money)) return true;

        return false;
        // here you have to compare current arguments with the ones remembered
    }

    @Override
    public Money convert(Money money, String targetCurrency) {
        if (money.getCurrency().equals(targetCurrency)) return money;

        double rate = 1.0/10;

        return new Money((int) (money.getAmount() / rate), targetCurrency);
    }

    @Override
    public String getAccountCurrency(String account) {
        switch (account) {
            case "E_123": return "EUR";
            case "S_456": return "SEK";
            default: throw new IllegalStateException();
        }
    }

    private boolean areFundsSufficient = true;
    @Override
    public boolean hasSufficientFundsFor(Money requiredAmount, String account) {

        return areFundsSufficient;
    }


    //TODO - if areFundsAvailable == true => hasSufficientFundsFor = true; else return;

    public void setSufficientFundsAvailable(boolean areFundsAvailable) {
        // at the moment hasSufficientFundsFor() returns always true.
        // this method is for configuring how hasSufficientFundsFor() responds.

        areFundsSufficient = areFundsAvailable;
    }


    public boolean wasWithdrawCalled() {
        // This method should say whether withdraw() method was called
        // (without considering arguments)

        if (wMoney!=null)return true;
        return false;

    }

}

/*
#setSufficientFundsAvailable
#hasSufficientFundsFor
withdraw called with: 1 EUR and E_123
#withdraw
deposit called with: 10 SEK and S_456
#deposit
#wasWithdrawCalled
 */