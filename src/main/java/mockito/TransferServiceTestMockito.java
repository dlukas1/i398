package mockito;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.*;
import static org.mockito.Mockito.*;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import common.BankService;
import common.TransferService;
import common.Money;

@SuppressWarnings("unused")


public class TransferServiceTestMockito {

    BankService bankService = mock(BankService.class);
    TransferService transferService = new TransferService(bankService);

    @Test
    public void transferWithCurrencyConversion() {

        when(bankService.getAccountCurrency("E_123")).thenReturn("EUR");
        when(bankService.getAccountCurrency("S_456")).thenReturn("SEK");
        when(bankService.convert(new Money(1,"EUR"), "SEK")).thenReturn(new Money(10,"SEK"));
        when(bankService.convert(new Money(1,"EUR"),"EUR")).thenReturn(new Money(1,"EUR"));
        when(bankService.hasSufficientFundsFor(new Money(1,"EUR"),"E_123")).thenReturn(true);

        transferService.transfer(new Money(1, "EUR"), "E_123", "S_456");

        verify(bankService).withdraw(new Money(1, "EUR"), "E_123");
        verify(bankService).deposit(new Money(10, "SEK"), "S_456");
    }

    @Test
    public void transferWhenNotEnoughFunds() {
        when(bankService.hasSufficientFundsFor(new Money(1,"EUR"),"E_123")).thenReturn(false);
        transferService.transfer(new Money(1, "EUR"), "E_123", "S_456");
        verify(bankService, Mockito.times(0)).withdraw(new Money(1, "EUR"), "E_123");
    }

    private Money anyMoney() {
        return (Money) any();
    }

    private String anyAccount() {
        return anyString();
    }
}