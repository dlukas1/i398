package person;

public class PersonBuilder {

    private String name;
    private int age;
    private String address;
    private String mail;
    private String gender;
    private String town;

    public PersonBuilder withAge(int age) {
        this.age = age;
        return this;
    }

    public PersonBuilder withGender (String gender){
        this.gender = gender;
        return this;
    }

    public PersonBuilder withTown (String town){
        this.town = town;
        return this;
    }

    public Person build() {
        Person person = new Person();
        person.setAge(age);
        person.setGender(gender);
        person.setAddress(new Address("",town));
        return person;
    }
}
