package person;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static java.util.Arrays.*;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;

public class PersonUtilTest {

    private PersonUtil personUtil = new PersonUtil();


    @Test
    public void findsOldestPerson() {
        Person p1 = aPerson().withAge(32).build();
        Person p2 = aPerson().withAge(55).build();
        Person p3 = aPerson().withAge(21).build();

        assertThat(personUtil.getOldest(asList(p1, p2, p3)), is((p2)));
    }


    @Test
    public void findsPersonsInLegalAge() {
        Person p1 = aPerson().withAge(12).build();
        Person p2 = aPerson().withAge(33).build();
        Person p3 = aPerson().withAge(18).build();

        assertThat(personUtil.getPersonsInLegalAge(asList(p1, p2, p3)), is(asList(p2, p3)));
    }

    @Test
    public void findsWomen() {
        Person jack = aPerson().withGender("M").build();
        Person jill = aPerson().withGender("F").build();
        Person jane = aPerson().withGender("F").build();

        assertThat(personUtil.getWomen(asList(jack,jill,jane)), is(asList(jill,jane)));

    }

    @Test
    public void findsPersonsLivingInSpecifiedTown() {
        Person p1 = aPerson().withTown("Tallinn").build();
        Person p2 = aPerson().withTown("Tartu").build();
        Person p3 = aPerson().withTown("Parnu").build();

        assertThat(personUtil.getPersonsWhoLiveIn("Tartu", asList(p1,p2,p3)),is(asList(p2)));
    }


    @Test
    public void testListOfNullPersons(){
        Person p1 = new Person();
        Person p2 = new Person();
        Person p3 = new Person();

        assertThat(personUtil.getOldest(asList(p1, p2, p3)), is(p1)); //just first person from list
        assertThat(personUtil.getPersonsInLegalAge(asList(p1, p2, p3)), is(empty()));
        assertThat(personUtil.getWomen(asList(p1, p2, p3)), is(empty()));
        assertThrows(IllegalStateException.class, () -> {
            throw new IllegalStateException();
        });
    }


    private PersonBuilder aPerson() {
        return new PersonBuilder();
    }
}
