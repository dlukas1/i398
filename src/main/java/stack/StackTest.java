package stack;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

public class StackTest {

    @Test
    public void newStackHasNoElements() {

         Stack stack = new Stack(100);
         assertThat(stack.getSize(), is(0));
    }

    @Test
    public void pushNewElements() {

        Stack stack = new Stack(100);
        stack.push(1);
        stack.push(5);
        stack.push(9);
        assertThat(stack.getSize(), is(3));
    }

    @Test
    public void peek() {

        Stack stack = new Stack(100);
        stack.push(1);
        stack.push(5);
        stack.push(9);
        int peekElement = stack.peek();
        assertThat(stack.peek(), is(9));
    }

    @Test
    public void pop() {

        Stack stack = new Stack(100);
        stack.push(1);
        stack.push(5);
        stack.push(9);
        int poppedElement = stack.pop();
        int topmostElement = stack.peek();
        int size = stack.getSize();
        //assertThat(stack.pop(), is(9) );
        assertThat(stack.getSize(), is(2));
    }

    @Test
    public void push2poop2remind0() {

        Stack stack = new Stack(100);
        stack.push(1);
        stack.push(5);
        stack.pop();
        stack.pop();
        int size = stack.getSize();
        assertThat(stack.getSize(), is(0));
    }

    //loo pinu, lisa (push) kaks elementi, võta (pop) kaks elementi ja kontrolli, et
    //need on needsamad lisatud elemendid
    @Test
    public void checkIsSame() {

        Stack stack = new Stack(100);
        stack.push(1);
        stack.push(5);

        int pushed1 = stack.pop();
        assertThat(pushed1, is(5));
        int pushed2 = stack.pop();
        assertThat(pushed2, is(1));
    }

}

