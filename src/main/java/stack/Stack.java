package stack;

public class Stack {

    public int [] stack;
    int HowManyElements = 0;

    public Stack(int size)
    {
        stack = new int[size];
    }

    public int getSize (){
        return HowManyElements;
    }

    //
    public int push (int newElement){
        stack[HowManyElements] = newElement;
        HowManyElements++;
        return HowManyElements;
    }

    //returns last element
    public int peek(){
        int lastElement = stack[HowManyElements-1];//HME is number of elements, but index of last is HME-1
        return lastElement;
    }

    //returns last & delete it
    public int pop(){
        int lastElement = stack[HowManyElements-1];
        HowManyElements--;
        return lastElement;
    }

}


/*- loo pinu, lisa (push) kaks elementi, võta (pop) kaks elementi ja kontrolli, et
  need on needsamad lisatud elemendid.
- loo pinu, lisa (push) kaks elementi, vaata pealmist elementi (peek) ja kontrolli,
  et see on õige element.
- loo pinu, lisa (push) kaks elementi, vaata pealmist elementi ja kontrolli, et
  pinus on 2 elementi.
- loo pinu, lisa (push) kaks elementi, vaata pealmist elementi, vaata uuesti pealmist
  elementi ja kontrolli, et see on seesama, mis esimesel korral.
- loo pinu, võta (pop) üks element ja kontrolli, et pinu viskab IllegalStateException-i.
- loo pinu, vaata pealmist elementi ja kontrolli, et pinu viskab IllegalStateException-i.*/