package invoice;


import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static invoice.helperMethods.countQuantityOfPartialPayment;
import static invoice.helperMethods.getDatesAsString;
import static invoice.helperMethods.getPaymentsAsString;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static org.mockito.Mockito.*;
import static org.mockito.hamcrest.MockitoHamcrest.argThat;

public class InvoiceRowGeneratorTest {

    @Mock InvoiceRowDao dao;

    @Test
    public void checkGeneratedDatesWith2periods() {
        InOrder inOrder = inOrder(dao);

        BigDecimal amount = new BigDecimal(100);
        LocalDate periodStart = asDate("2017-01-01");
        LocalDate periodEnd = asDate("2017-02-02");

        InvoiceRowGenerator generator = new InvoiceRowGenerator(dao);
        generator.generateInvoiceRows(amount, periodStart, periodEnd);

        inOrder.verify(dao).save(argThat(getMatcherForDate("2017-01-01")));
        inOrder.verify(dao).save(argThat(getMatcherForDate("2017-02-01")));
    }

    @Test
    public void checkGeneratedDatesWith3periods() {
        InOrder inOrder = inOrder(dao);

        BigDecimal amount = new BigDecimal(11);
        LocalDate periodStart = asDate("2017-01-02");
        LocalDate periodEnd = asDate("2017-03-01");

        InvoiceRowGenerator generator = new InvoiceRowGenerator(dao);
        generator.generateInvoiceRows(amount, periodStart, periodEnd);

        inOrder.verify(dao).save(argThat(getMatcherForDate("2017-01-02")));
        inOrder.verify(dao).save(argThat(getMatcherForDate("2017-02-01")));
        inOrder.verify(dao).save(argThat(getMatcherForDate("2017-03-01")));
    }

    @Test
    public void checkGeneratedPaymentsWithoutReminder(){
        InOrder inOrder = inOrder(dao);

        BigDecimal amount = new BigDecimal(9);
        LocalDate periodStart = asDate("2017-01-02");
        LocalDate periodEnd = asDate("2017-03-01");

        InvoiceRowGenerator generator = new InvoiceRowGenerator(dao);
        generator.generateInvoiceRows(amount, periodStart, periodEnd);

        inOrder.verify(dao, times(3)).save(argThat(getMatcherForAmount(3)));
    }

    @Test
    public void checkGeneratedPaymentsWithReminder(){
        InOrder inOrder = inOrder(dao);

        BigDecimal amount = new BigDecimal(11);
        LocalDate periodStart = asDate("2017-01-02");
        LocalDate periodEnd = asDate("2017-03-01");

        InvoiceRowGenerator generator = new InvoiceRowGenerator(dao);
        generator.generateInvoiceRows(amount, periodStart, periodEnd);

        inOrder.verify(dao).save(argThat(getMatcherForAmount(3)));
        inOrder.verify(dao, times(2)).save(argThat(getMatcherForAmount(4)));
    }

    @Test
    public void checkGeneratedCustomPayments(){
        InOrder inOrder = inOrder(dao);

        BigDecimal amount = new BigDecimal(11);
        LocalDate periodStart = asDate("2017-01-02");
        LocalDate periodEnd = asDate("2017-05-01");

        InvoiceRowGenerator generator = new InvoiceRowGenerator(dao);
        generator.generateInvoiceRows(amount, periodStart, periodEnd);

        inOrder.verify(dao).save(argThat(getMatcherForAmount(3)));
        inOrder.verify(dao, times(2)).save(argThat(getMatcherForAmount(4)));

    }

    @Test
    public void checkGeneratedPaymentsWithSmallPayments(){
        InOrder inOrder = inOrder(dao);

        BigDecimal amount = new BigDecimal(7);
        LocalDate periodStart = asDate("2017-01-02");
        LocalDate periodEnd = asDate("2017-04-01");

        InvoiceRowGenerator generator = new InvoiceRowGenerator(dao);
        generator.generateInvoiceRows(amount, periodStart, periodEnd);

        inOrder.verify(dao).save(argThat(getMatcherForAmount(3)));
        inOrder.verify(dao).save(argThat(getMatcherForAmount(4)));
    }

    @Test
    public void checkGeneratedPaymentsWithSmallAmount(){
        InOrder inOrder = inOrder(dao);

        BigDecimal amount = new BigDecimal(2);
        LocalDate periodStart = asDate("2017-01-02");
        LocalDate periodEnd = asDate("2017-03-01");

        InvoiceRowGenerator generator = new InvoiceRowGenerator(dao);
        generator.generateInvoiceRows(amount, periodStart, periodEnd);

        inOrder.verify(dao).save(argThat(getMatcherForAmount(2)));

    }





    @BeforeEach
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    private Matcher<InvoiceRow> getMatcherForAmount(final Integer amount) {
        // Example matcher for testing that argument has certain amount.

        return new TypeSafeMatcher<InvoiceRow>() {

            @Override
            protected boolean matchesSafely(InvoiceRow item) {
                return amount.equals(item.amount.intValue());
            }

            @Override
            public void describeTo(Description description) {
                String message = MessageFormat.format(
                        "InvoiceRow with amount {0}", amount);

                description.appendText(message);
            }
        };
    }

    private Matcher<InvoiceRow> getMatcherForDate(String localDate) {

        return new TypeSafeMatcher<InvoiceRow>() {

            @Override
            protected boolean matchesSafely(InvoiceRow item) {
                return asDate(localDate).equals(item.date);
            }

            @Override
            public void describeTo(Description description) {
                String message = MessageFormat.format(
                        "InvoiceRow with date {0}", localDate);

                description.appendText(message);
            }
        };
    }

    private LocalDate asDate(String string) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        return LocalDate.parse(string, formatter);
    }

}