package invoice;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import static invoice.helperMethods.countQuantityOfPartialPayment;
import static java.lang.Math.toIntExact;
import static java.time.temporal.ChronoUnit.MONTHS;


public class InvoiceRowGenerator {

    private InvoiceRowDao dao;

    public InvoiceRowGenerator(InvoiceRowDao dao){
        this.dao = dao;
    }

    public InvoiceRowGenerator(){}

    MathContext mc = new MathContext(2);

    public void generateInvoiceRows(BigDecimal amount, LocalDate periodStart, LocalDate periodEnd){

        //How many months between periodStart and periodEnd
        BigDecimal quantOfPeriods = helperMethods.countQuantityOfPartialPayment( periodStart, periodEnd);

        //Calculate whole number of monthly payment, round_down to get rid of reminder
        BigDecimal payment = amount.divide(quantOfPeriods,2, BigDecimal.ROUND_DOWN);

        //if total amount is less than 3 - make single payment at the end of period (1st day of last month)
        if (amount.intValue()<3){
            calcSinglePayment(amount, periodEnd);
        }

        //if monthly payment is less than 3 - combine with neighbor payment
        else if(payment.intValue()<3){
            generateCustomInvoiceRows(amount, periodStart);
        }

        else {

            int remains = amount.remainder(quantOfPeriods, mc).intValue();
            //initialRow contains periodStart date for first payment
            InvoiceRow initialRow = new InvoiceRow(payment.setScale(0,BigDecimal.ROUND_DOWN), periodStart);
            dao.save(initialRow);

            //for other payments date is first day of month
            for (int i = 1; i < quantOfPeriods.intValue(); i++) {

                //last payments contains reminder
                if(remains!=0 && i>=quantOfPeriods.intValue() -remains){
                    InvoiceRow invoiceRow = new InvoiceRow(payment.add(new BigDecimal(1)).setScale(0,BigDecimal.ROUND_DOWN), periodStart.plusMonths(i));
                    dao.save(invoiceRow);
                }

                InvoiceRow invoiceRow = new InvoiceRow(payment.setScale(0,BigDecimal.ROUND_DOWN), helperMethods.setFirstDay(periodStart.plusMonths(i)));
                dao.save(invoiceRow);
            }
        }
    }


    public void calcSinglePayment (BigDecimal amuont, LocalDate periodEnd){

        InvoiceRow invoiceRow = new InvoiceRow(amuont,helperMethods.setFirstDay(periodEnd));
        dao.save(invoiceRow);
    }

    public void generateCustomInvoiceRows (BigDecimal amuont, LocalDate periodStart){

        //Smallest possible payment is 3
        BigDecimal minPayment = new BigDecimal("3");


        BigDecimal periods = amuont.divide(minPayment, BigDecimal.ROUND_DOWN);

        if (periods.intValue()==1){
            InvoiceRow aloneRow = new InvoiceRow(amuont, periodStart);
            dao.save(aloneRow);
        }

        else{
            BigDecimal payment = amuont.divide(periods, BigDecimal.ROUND_DOWN);
            int remains = amuont.remainder(periods, mc).intValue();
            InvoiceRow initialRow = new InvoiceRow(payment, periodStart);
            dao.save(initialRow);

            for (int i = 1; i < periods.intValue(); i++) {
                InvoiceRow invoiceRow = new InvoiceRow(payment, helperMethods.setFirstDay(periodStart.plusMonths(i)));

                //last payment contains reminder
                if(remains !=0 && i>=periods.intValue() -remains){
                    invoiceRow = new InvoiceRow(payment.add(new BigDecimal(1)), periodStart.plusMonths(i));
                    dao.save(invoiceRow);
                }
            }
        }
    }
}
