Iseseisev töö 2

Kirjutage testidel põhinevalt kood, mis genereerib osamakseid ja
salvestab need andmbebaasi. Sisendiks on summa ja periood, millele see
summa jagada tuleb. Väljundiks on andmebaasi salvestatud osamaksed.

Päris andmebaasi ei tohi kasutada. Andmebaasi sõltuvuse eemaldamiseks
kasutage Mockito raamistikku.

Milliseid klasse ja meetodeid vajate otsustage ise. Oluline on, et
salvestamine toimuks läbi InvoiceRowDao liidese ja
arverea infot hoitakse InvoiceRow klassis.

Vajalik kood:
  https://bitbucket.org/mkalmo/i398/src/master/src/main/java/invoice/

Seda, kas õiged andmed baasi kirjutatakse saate kontrolida selle järgi,
et InvoiceRowDao.save() meetodit õige argumendiga välja kutsutakse.

Osamakseteks jagamine toimub järgmiste reeglite alusel:
  - esimene osamakse tuleb perioodi esimesel päeval;
  - ülejäänud osamaksed tulevad kuu esimesel päeval;
  - kui summa ei jagu Euro täpsusega, siis jaotatakse vahe viimaste
    osamaksete vahel;
  - kahte osamakset ühele päevale sattuda ei või;
  - miinimum osamakse summa on 3 EUR-i.

Nt. kui jagatav summa on 10 ja periood on 2017-02-15 kuni 2017-04-02,
siis tulevad järgmised osamaksed (summa - kuupäev):

  3 - 2017-02-15
  3 - 2017-03-01
  4 - 2017-04-01 (jääk läheb viimasele osamaksele)

Vajalikud on vähemalt järgmised testid:

  1. kuupäevad on õiged
      2017-01-01 kuni 2017-02-02 -> 2017-01-01, 2017-02-01
      2017-01-02 kuni 2017-03-01 -> 2017-01-02, 2017-02-01, 2012-03-01
  2. summa jagatakse õigesti
      9 kolmeks -> 3, 3, 3
  3. summa jagatakse õigesti (ei jagu täpselt)
      11 kolmeks -> 3, 4, 4
  4. osamakse summa tuleks väiksem kui 3 EUR-i (pannakse teistega kokku)
      7 neljaks -> 3, 4
  5. summa on alla 3 EUR-i, siis jääb üks vastava summaga osamakse
      2 kolmeks -> 2

Summa kontrollimise testid (testid 2-5) ei tohiks kuupäevi kontrollida.

Vältige duplikatsiooni. Kui mitu väga sarnast rida on järjest, siis
võiks sellega midagi ette võtta. Samas ei tohiks loetavus selle all kannatada.