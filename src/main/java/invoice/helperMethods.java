package invoice;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import static java.lang.Math.toIntExact;
import static java.time.temporal.ChronoUnit.MONTHS;

public class helperMethods {


    public static LocalDate setFirstDay(LocalDate date){
        LocalDate firstDayOfMonth =  date.withDayOfMonth(1);
        return firstDayOfMonth;
    }

    public static BigDecimal countQuantityOfPartialPayment (LocalDate periodStart, LocalDate periodEnd){
        periodStart = periodStart.withDayOfMonth(1);
        periodEnd = periodEnd.withDayOfMonth(1);
        return new BigDecimal(MONTHS.between(periodStart, periodEnd) + 1) ;
    }

    public static String getDatesAsString(List<InvoiceRow> invoiceRows){
        String dates = "";
        for(InvoiceRow invoiceRow : invoiceRows){
            dates += invoiceRow.getDate() +" ";
        }
        return dates;
    }

    public static String getPaymentsAsString(List <InvoiceRow> invoiceRows){
        String payments = "";
        for(InvoiceRow invoiceRow : invoiceRows){
            payments += invoiceRow.getAmount() +" ";
        }
        return payments;
    }
}
