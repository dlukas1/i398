package string;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static string.OrderServiceTest.asDate;

@SuppressWarnings("unused")
public class OrderService {



    private DataSource dataSource;

    public OrderService(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    //List<Order> getFilledOrders() - tagastab täidetud (filled) tellimused
    public List<Order> getFilledOrders()
    {
        OrderServiceTest ost = new OrderServiceTest();
        List<Order> orders = ost.getOrders().dataSource.getOrders();
        List<Order> filledOrders = new ArrayList<>();

        for (Order order :orders)
        { if (order.isFilled()==true)
            { filledOrders.add(order); }
        }
        return filledOrders;
    }

    //List<Order> getOrdersOver(double amount) - tagastab tellimused, mis ületavad argumendina etteandud summat.
    public List<Order> getOrdersOver(double amount)
    {
        OrderServiceTest ost = new OrderServiceTest();
        List<Order> orders = ost.getOrders().dataSource.getOrders();
        List<Order> ordersOverAmount = new ArrayList<>();
        for(Order order : orders)
        {
            if(order.getTotal()>amount)
            {ordersOverAmount.add(order);}
        }
        return ordersOverAmount;
    }

    public List<Order> getOrdersSortedByDate()
    {
        OrderServiceTest ost = new OrderServiceTest();
        List<Order> orders = ost.getOrders().dataSource.getOrders();
        Collections.sort(orders, new Comparator<Order>() {
            @Override
            public int compare(Order o1, Order o2) {
                return o1.getOrderDate().compareTo(o2.getOrderDate());
            }
        });
        return orders;
    }
}
