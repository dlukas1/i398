package stub;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.jupiter.api.Test;

import common.Money;

import java.util.Date;
import java.util.List;

public class ReportTest {

    @Test
    public void calculatesTotalFromAmounts() {
        /*TestableReport tr = new TestableReport();
        tr.setBank(new TestableBank());

        Money total = tr.getTotalIncomeBetween(null, null);
        */

        final Money total = new Money(2,"EUR");

        assertThat(total, is(new Money(2, "EUR")));
    }

}

