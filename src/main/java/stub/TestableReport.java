package stub;

import common.Money;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class TestableReport extends Report {

    @Override
    public Money getTotalIncomeBetween(Date startDate, Date endDate) {

        return new Money(2,"EUR");
    }
}
